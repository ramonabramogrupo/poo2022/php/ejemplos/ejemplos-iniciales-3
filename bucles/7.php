<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $enlaces = [
            [
                "label" => "Buscador google",
                "link" => "https://www.google.es",
            ],
            [
                "label" => "Buscador Bing",
                "link" => "https://www.bing.com",
            ],
            [
                "label" => "Pagina web de Alpe",
                "link" => "https://alpeformacion.es",
            ]
        ];
        
        // comienzo el menu
        $c=0;
        while($c<count($enlaces)){
        ?>
        <div>
            <a href="<?= $enlaces[$c]["link"]?>">
                <?= $enlaces[$c]["label"]?>
            </a>
        </div>
        
        <?php
        $c++;
        }
        // fin del menu
        ?>
    </body>
</html>
