<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // array con numeros
        $a=[1,2,3,4];
        
        // imprimir el array con un foreach
        
        foreach ($a as $indice => $valor) {
            echo "En la posicion {$indice} el valor es {$valor}<br>";
        }
        ?>
    </body>
</html>
