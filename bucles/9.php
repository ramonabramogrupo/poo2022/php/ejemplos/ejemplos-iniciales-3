<?php
$elementos=[
    [
        "color" => "red",
        "ancho" => "300px",
        "alto" => "20px",
        "texto" => "ejemplo 1"
    ],
    [
        "color" => "blue",
        "ancho" => "500px",
        "alto" => "50px",
        "texto" => "ejemplo 2"
    ],
    [
        "color" => "green",
        "ancho" => "200px",
        "alto" => "30px",
        "texto" => "ejemplo 3"
    ],
];
        
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // principio bucle
        foreach ($elementos as $indice=>$elemento){
        ?>
        
        <div style="background-color:<?= $elemento["color"] ?>;width:<?= $elemento["ancho"] ?>;height:<?= $elemento["alto"] ?>">
           <?= $elemento["texto"] ?> 
        </div>
        <br>
        
        <?php
        }
        // fin del bucle
        ?>
        
    </body>
</html>
