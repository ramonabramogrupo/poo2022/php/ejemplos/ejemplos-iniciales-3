<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // el calculo del descuento depende de la cantidad
        // cantidad <1.000 ==> descuento=1%
        // cantidad >=1.000 y <10.000 ==> descuento 2%
        // cantidad >=10.000 ==> descuento 5%
        
        $cantidad=190000;
                        
        if($cantidad<1000){
            $descuento=1/100;
        }elseif ($cantidad<10000) {
            $descuento=2/100;
        }else{
            $descuento=5/100;
        }
        
        echo $descuento;
        
        
        switch (true){
            case ($cantidad<1000):
                $descuento=1/100;
                break;
            case ($cantidad<10000):
                $descuento=2/100;
                break; 
            default :
                $descuento=5/100;
        }
        
        echo $descuento;
        ?>
    </body>
</html>
