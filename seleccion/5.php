<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // pedir al api 3 numeros
        // mediante if que me indique cual de los 3 numeros es mayor
        
        $url="http://localhost/poo2022/apiRest/ejemplo1restyii/web/index.php/site/numeros?cantidad=3";
        
        $texto=file_get_contents($url);
        
        $numeros= json_decode($texto);
        var_dump($numeros);
        
        $mayor=0;
        if ($numeros[0]>$numeros[1]) {
            if ($numeros[0]>$numeros[2]) {
                $mayor=$numeros[0];
            } else {
                $mayor=$numeros[2];
            }
        }else{
            if($numeros[1]>$numeros[2]){
                $mayor=$numeros[1];
            }else{
                $mayor=$numeros[2];
            }
        }
        
        echo $mayor;
        echo "<hr>";
        
        // opcion 2
        // utilizando sort
        
        rsort($numeros); // ordeno el array descendente
        var_dump($numeros);
        $mayor=$numeros[0];
        echo $mayor;
        echo "<hr>";
        
        // opcion 3
        // utilizando max
        $mayor=max($numeros);
        echo $mayor;
        
        
        
        
        ?>
    </body>
</html>
