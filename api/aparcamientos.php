<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>PISCINAS</title>
    </head>
    <body>
        <?php
        $url="https://datos.madrid.es/egob/catalogo/201132-0-museos.json";
        $texto=file_get_contents($url);
        $salida= json_decode($texto);
        $piscinas=$salida->{"@graph"};
        
        foreach ($piscinas as $piscina){
            
?>
        <div style="margin:10px">
        <a href="<?= $piscina->relation ?>"><?= $piscina->title ?></a>
        </div>
<?php
        }
        
        ?>
        
    </body>
</html>
