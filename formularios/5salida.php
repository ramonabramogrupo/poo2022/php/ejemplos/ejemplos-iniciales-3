<?php
    function promedio($datos){
        $suma=array_sum($datos);
        $cantidad=count($datos);
        $media=$suma/$cantidad;
        return $media;
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // creo una variable que sera un array con los 3 numeros
        $numeros=$_GET["numeros"];
        //$numeros=[2,4,6];
        
        // calcular la suma (array_sum)
        echo array_sum($numeros);
         
        echo "<br>";
        
        // calcular el producto (array_product)
        echo array_product($numeros);
        
        echo "<br>";
        
        // calcular la media (promedio)
        echo promedio($numeros);
        
        ?>
    </body>
</html>
