<?php
    // Cuando pulses el boton de enviar quiero que me calcule la 
    // suma, el producto y la media de los 3 numeros
    // para calcular la suma utilizamos array_sum
    // para calcular el producto utilizamos array_product
    // para la calcular la media utilizo una funcion  creada
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            // colocar un formulario para introducir 3 numeros
        ?>
        <form action="5salida.php">
            <div>
                <label for="numero1">Numero 1</label>
                <input type="number" name="numeros[]" id="numero1">
            </div>
            <div>
                <label for="numero2">Numero 2</label>
                <input type="number" name="numeros[]" id="numero2">
            </div>
            <div>
                <label for="numero3">Numero 3</label>
                <input type="number" name="numeros[]" id="numero3">
            </div>
            <div>
                <button>Calcular</button>
            </div>
        </form>
    </body>
</html>
