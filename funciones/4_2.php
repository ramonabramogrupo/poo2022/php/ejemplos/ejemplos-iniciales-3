<?php
// funcion sumar con numero de argumentos variable
    
    /**
     * Suma los numeros pasados como argumentos
     * @param array $numeros Array de numeros enteros
     * @return int Suma de los numeros
     */
    function sumar($numeros){
        $resultado=0;
        foreach ($numeros as $numero){
            $resultado+=$numero; // $resultado=$resultado+$numero
        }
        
//        for($c=0;$c<count($numeros);$c++){
//            $resultado+=$numeros[$c];
//        }
        
        return $resultado;
        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo sumar([1,2,3,4]);
        echo "<br>";
        echo sumar([1,2]);
        ?>
    </body>
</html>
