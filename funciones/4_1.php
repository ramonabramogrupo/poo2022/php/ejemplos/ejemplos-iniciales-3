<?php
// funcion sumar con numero de argumentos variable
    /**
     * Funcion que suma los numeros pasados como argumento
     * @return int suma de todos los numeros pasados
     */
    function sumar(){
        $numeros= func_get_args();
        $resultado=0;
        foreach ($numeros as $numero){
            $resultado+=$numero; // $resultado=$resultado+$numero
        }
        
//        for($c=0;$c<count($numeros);$c++){
//            $resultado+=$numeros[$c];
//        }
        
        return $resultado;
        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo sumar(1,2,3,4);
        echo "<br>";
        echo sumar(1,2);
        ?>
    </body>
</html>
