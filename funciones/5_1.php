<?php

    function promedio(){
        $numeros= func_get_args();
        $resultado=0;
        $suma=0;
        
        // calculando numero de elementos
        $cantidad=count($numeros);
        
        // calcular suma
//        foreach ($numeros as $numero){
//            $suma+=$numero;
//        }
//        
        // calcular suma
        $suma= array_sum($numeros);
        
        // calculo media
        $resultado=$suma/$cantidad;
        
        // devolviendo media
        return $resultado;
        
        // esta instruccion hace todo
        //return array_sum($numeros)/count($numeros);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo promedio(2,4); // 3
        echo "<br>";
        echo promedio(3,6,9); // 6
        
        ?>
    </body>
</html>
